import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:practical_test/models/repo.dart';
import 'package:practical_test/notifiers/repo_view_model.dart';
import 'package:practical_test/utils/PrefUtil.dart';
import 'package:practical_test/utils/app_constant.dart';

class ApiService {
  static const String API_ENDPOINT =
      "https://api.github.com/search/repositories";

  static getRepos(RepoNotifierViewModel refoNotifier, int pageIndex,
      BuildContext context) async {
    Repo? refo;
    refoNotifier.setLoading(true);
    http
        .get(
      Uri.parse(
          "${API_ENDPOINT}?q=Flutter&per_page=10&page=${pageIndex.toString()}&sort=stars&order=desc"),
    )
        .then((response) {
      print('Response status: ${response.statusCode}');
      print('Response Page Index: ${pageIndex}');

      if (response.statusCode == 200) {
        print('Response body: ${response.body}');
        Map repos = jsonDecode(response.body);
        refo = Repo.fromJson(repos);
        //print(refo!.items!);
        refoNotifier.setRepoList(refo!.items!, pageIndex);
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("Data fetch successfully!")));

        PrefUtil.preferences!.setInt(AppConstant.CURRENT_REPO_PAGE_INDEX, pageIndex + 1);
        PrefUtil.preferences!.setString(AppConstant.API_REQUEST_TIME, DateTime.now().toString());

      } else {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Error'),
            content: Text('Failed to fetch data. Please try again later.'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          ),
        );
      }
      refoNotifier.setLoading(false);
    });
  }
}
