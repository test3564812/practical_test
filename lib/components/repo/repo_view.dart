import 'package:flutter/material.dart';
import 'package:practical_test/components/repo/repo_view_model.dart';
import 'package:practical_test/models/repo.dart';
import 'package:practical_test/utils/common_operation.dart';
import 'package:practical_test/utils/widgets.dart';

class RepoView extends StatefulWidget {
  final Items refo;
  final int index;

  RepoView({@required required this.refo, @required required this.index});

  @override
  State createState() {
    return RepoViewState(refo);
  }
}

class RepoViewState extends State<RepoView> {
  late Items refo;
  late RefoViewModel repoViewModel;
  late Size? newSize;

  RepoViewState(this.refo) {
    repoViewModel = new RefoViewModel();
    repoViewModel.setRepo(refo);
  }

  @override
  Widget build(BuildContext context) {
    newSize = MediaQuery.sizeOf(context);
    return Card(
      margin: EdgeInsets.all(8),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () async {
            Future.delayed(Duration(microseconds: 800), () {
              Navigator.pushNamed(context, "/repo_details_view",
                  arguments: refo);
            });
          },
          child: new Container(
            height: 90,
            child: Container(
              child: Padding(
                padding: EdgeInsets.only(left: 8),
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              child: CustomWidgets.customTV(
                                  repoViewModel.refo.owner!.login!,
                                  font_size: 14),
                            ),
                            Container(
                              child: CustomWidgets.customTV(
                                  "/" + repoViewModel.refo.name!,
                                  font_size: 14),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        CustomWidgets.customTV(
                            "Updated: " +
                                CommonOperation.getLocalTime(
                                    repoViewModel.refo.updatedAt!),
                            font_size: 12,
                            color: Colors.grey),
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomWidgets.customTV(
                                "Star :" +
                                    repoViewModel.refo.stargazersCount!
                                        .toString(),
                                font_size: 12,
                                color: Colors.grey),
                          ],
                        ),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CustomWidgets.customTV(
                                font_size: 12,
                                color: Colors.grey,
                                (widget.index + 1).toString()),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
