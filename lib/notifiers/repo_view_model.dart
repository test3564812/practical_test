import 'dart:io';

import 'package:flutter/material.dart';
import 'package:practical_test/db/db_helper.dart';
import 'package:practical_test/models/repo.dart';
import 'package:practical_test/services/api_service.dart';
import 'package:practical_test/utils/PrefUtil.dart';
import 'package:practical_test/utils/app_constant.dart';
import 'package:practical_test/utils/common_operation.dart';

class RepoNotifierViewModel with ChangeNotifier {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Items> _repoList = [];
  bool _isLoading = false;
  bool _hasMore = false;

  DBHelper dbHelper = new DBHelper();

  RepoNotifierViewModel() {}

  setRepoList(List<Items> repoList, int pageIndex) async {
    _repoList.addAll(repoList);
    bool value = await this.dbHelper.getIsPageExistInDb(pageIndex);
    if (!value) dbHelper.add(repoList, pageIndex);
    sortBy();
    notifyListeners();
  }

  sortBy() {
    String _value =
        PrefUtil.preferences!.getString(AppConstant.SORT_BY) ?? 'Sort By';
    if (_value.contains("Date")) {
      _repoList.sort((a, b) => a.updatedAt!.compareTo(b.updatedAt!));
    } else if (_value.contains("Star")) {
      _repoList
          .sort((a, b) => a.stargazersCount!.compareTo(b.stargazersCount!));
    }

    notifyListeners();
  }

  setLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool getLoading() {
    return _isLoading;
  }

  setHasMore(bool value) {
    _hasMore = value;
    notifyListeners();
  }

  bool getHasMore() {
    return _hasMore;
  }

  List<Items> getRepoList() {
    return _repoList;
  }

  Future<void> fetchData(BuildContext context) async {
    bool value = await this.hasNetwork();
    if (value) {
      fetchNetworkData(context);
    } else
      getDataFromLocal();
  }

  void getDataFromLocal() async {
    _repoList.clear();
    _repoList.addAll(await dbHelper.getRepoItem());
    sortBy();
    notifyListeners();
  }

  Future<void> fetchNetworkData(BuildContext context) async {
    int pageIndex =
        PrefUtil.preferences!.getInt(AppConstant.CURRENT_REPO_PAGE_INDEX) ?? 1;
    ApiService.getRepos(this, pageIndex, context);
  }

  Future<bool> hasNetwork() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
    } on SocketException catch (_) {
      return false;
    }
  }
}
