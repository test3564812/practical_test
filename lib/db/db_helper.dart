import 'package:practical_test/models/repo.dart';
import 'package:practical_test/utils/common_operation.dart';
import 'package:practical_test/utils/db_constant.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class DBHelper {
  static Database? _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    }
    _db = await initDatabase();
    return _db!;
  }

  initDatabase() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, 'repo.db');
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    String sqlQuery =
        "CREATE TABLE ${DatabaseHelper.TABLE_NAME} (${DatabaseHelper.ID} INTEGER PRIMARY KEY, ${DatabaseHelper.NAME} TEXT, ${DatabaseHelper.LOGIN} TEXT, ${DatabaseHelper.AVATAR_URL} TEXT, ${DatabaseHelper.DESCRIPTION} TEXT, ${DatabaseHelper.STAR_COUNT} TEXT, ${DatabaseHelper.UPDATED_AT} TEXT, ${DatabaseHelper.PAGE_INDEX} TEXT)";
    print("object44${sqlQuery}");
    await db.execute(sqlQuery);
  }

  Future<void> add(List<Items> repoItem, int page) async {
    var dbClient = await db;
    repoItem.forEach((repo) async {
      await dbClient.rawInsert(
        'INSERT INTO ${DatabaseHelper.TABLE_NAME}('
        '${DatabaseHelper.ID}, '
        '${DatabaseHelper.NAME}, '
        '${DatabaseHelper.LOGIN}, '
        '${DatabaseHelper.AVATAR_URL}, '
        '${DatabaseHelper.DESCRIPTION}, '
        '${DatabaseHelper.STAR_COUNT}, '
        '${DatabaseHelper.UPDATED_AT}, '
        '${DatabaseHelper.PAGE_INDEX}) VALUES(?, ?, ?, ?, ?, ?, ?, ?)',
        [
          repo.id,
          repo.name,
          repo.owner!.login,
          repo.owner!.avatarUrl,
          repo.description,
          repo.stargazersCount,
          repo.updatedAt,
          page.toString()
        ],
      );
    });
  }

  Future<List<Items>> getRepoItem() async {
    var dbClient = await db;
    List<Map> maps =
        await dbClient.query('${DatabaseHelper.TABLE_NAME}', columns: [
      '${DatabaseHelper.ID}, '
          '${DatabaseHelper.NAME}, '
          '${DatabaseHelper.LOGIN}, '
          '${DatabaseHelper.AVATAR_URL}, '
          '${DatabaseHelper.DESCRIPTION}, '
          '${DatabaseHelper.STAR_COUNT}, '
          '${DatabaseHelper.UPDATED_AT}, '
          '${DatabaseHelper.PAGE_INDEX}'
    ]);

    /*int? id;
    String? name;
    String? fullName;
    Owner? owner;
    String? description;
    String? createdAt;
    String? updatedAt;
    String? pushedAt;
    int? stargazersCount;
    int? watchersCount;*/

    return List.generate(maps.length, (i) {
      return Items(
        id: maps[i]['${DatabaseHelper.ID}'],
        name: maps[i]['${DatabaseHelper.NAME}'],
        owner: Owner(
            avatarUrl: maps[i]['${DatabaseHelper.AVATAR_URL}'],
            login: maps[i]['${DatabaseHelper.LOGIN}']),
        description: maps[i]['${DatabaseHelper.DESCRIPTION}'],
        updatedAt: maps[i]['${DatabaseHelper.UPDATED_AT}'],
        stargazersCount: int.parse(maps[i]['${DatabaseHelper.STAR_COUNT}']),
      );
    });
  }

  Future<bool> getIsPageExistInDb(int pageIndex) async {
    var dbClient = await db;

    String whereString = '${DatabaseHelper.PAGE_INDEX} = ?';
    List<dynamic> whereArguments = [pageIndex];

    List<Map> maps = await dbClient.query('${DatabaseHelper.TABLE_NAME}',
        columns: [
          '${DatabaseHelper.ID}, '
              '${DatabaseHelper.NAME}, '
              '${DatabaseHelper.AVATAR_URL}, '
              '${DatabaseHelper.DESCRIPTION}, '
              '${DatabaseHelper.STAR_COUNT}, '
              '${DatabaseHelper.UPDATED_AT}, '
              '${DatabaseHelper.PAGE_INDEX}'
        ],
        where: whereString,
        whereArgs: whereArguments);

    print("object${maps.length}");

    return maps.length > 0 ? true : false;
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(
      '${DatabaseHelper.TABLE_NAME}',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  /*Future<int> update(Items items) async {
    var dbClient = await db;
    return await dbClient.update(
      '${DbConstant.TABLE_NAME}',
      items.toMap(),
      where: 'id = ?',
      whereArgs: [items.id],
    );
  }*/

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
