class Repo {
  int? totalCount;
  bool? incompleteResults;
  List<Items>? items;

  Repo({this.totalCount, this.incompleteResults, this.items});

  Repo.fromJson(Map<dynamic, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_count'] = this.totalCount;
    data['incomplete_results'] = this.incompleteResults;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  int? id;
  String? name;
  String? fullName;
  Owner? owner;
  String? description;
  String? createdAt;
  String? updatedAt;
  String? pushedAt;
  int? stargazersCount;
  int? watchersCount;

  Items(
      {this.id,
        this.name,
        this.fullName,
        this.owner,
        this.description,
        this.createdAt,
        this.updatedAt,
        this.pushedAt,
        this.stargazersCount,
        this.watchersCount});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    fullName = json['full_name'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pushedAt = json['pushed_at'];
    stargazersCount = json['stargazers_count'];
    watchersCount = json['watchers_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['full_name'] = this.fullName;
    if (this.owner != null) {
      data['owner'] = this.owner!.toJson();
    }
    data['description'] = this.description;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['pushed_at'] = this.pushedAt;
    data['stargazers_count'] = this.stargazersCount;
    data['watchers_count'] = this.watchersCount;
    return data;
  }

  @override
  String toString() {
    return 'Items{id: $id, name: $name, fullName: $fullName, owner: $owner, description: $description, createdAt: $createdAt, updatedAt: $updatedAt, pushedAt: $pushedAt, stargazersCount: $stargazersCount, watchersCount: $watchersCount}';
  }


}

class Owner {
  String? login;
  String? avatarUrl;

  Owner({this.login, this.avatarUrl});

  Owner.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    avatarUrl = json['avatar_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    data['avatar_url'] = this.avatarUrl;
    return data;
  }
}