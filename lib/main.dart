import 'package:flutter/material.dart';
import 'package:practical_test/screens/repo_details_view.dart';
import 'package:practical_test/notifiers/repo_view_model.dart';
import 'package:practical_test/screens/home_screen.dart';
import 'package:practical_test/screens/splash_screen.dart';
import 'package:practical_test/utils/PrefUtil.dart';

import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  PrefUtil.init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => RepoNotifierViewModel(),
      ),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  State createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
      routes: {
        "/home_screen": (context) => HomeScreen(),
        "/repo_details_view": (context) => RepoDetailsView(),
      },
    );
  }
}
