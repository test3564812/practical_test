import 'package:flutter/material.dart';
import 'package:practical_test/models/repo.dart';
import 'package:practical_test/utils/common_operation.dart';

class RepoDetailsView extends StatefulWidget {
  const RepoDetailsView({super.key});

  @override
  State<RepoDetailsView> createState() => _RepoDetailsViewState();
}

class _RepoDetailsViewState extends State<RepoDetailsView> {
  Items? items;

  @override
  Widget build(BuildContext context) {
    items = ModalRoute.of(context)?.settings.arguments as Items;
    print("https://github.com/${items!.owner!.login}/${items!.name}");
    return Scaffold(
      appBar: AppBar(title: Text("Repo Details"), elevation: 2),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 48, // Image radius
                  backgroundImage: NetworkImage("${items!.owner!.avatarUrl}"),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Text("Owner Name : ${items!.name}",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
            SizedBox(
              height: 10,
            ),
            GestureDetector(
                onTap: () {
                  /*launch(
                      "https://github.com/${items!.owner!.login}/${items!.name}");*/
                },
                child: Text(
                  "URL : https://github.com/${items!.owner!.login}/${items!.name}",
                  style: TextStyle(
                      color: Colors.lightBlue, fontWeight: FontWeight.w600),
                )),
            SizedBox(
              height: 10,
            ),
            Text("Description : ${items!.description}"),
            SizedBox(
              height: 10,
            ),
            Text(
                "Last Updated: ${CommonOperation.getLocalTime(items!.updatedAt!)}")
          ],
        ),
      ),
    );
  }
}
