import 'package:flutter/material.dart';
import 'package:practical_test/components/repo/repo_view.dart';
import 'package:practical_test/models/repo.dart';
import 'package:practical_test/notifiers/repo_view_model.dart';
import 'package:practical_test/services/api_service.dart';
import 'package:practical_test/utils/PrefUtil.dart';
import 'package:practical_test/utils/app_constant.dart';
import 'package:practical_test/utils/common_operation.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomeScreen> {
  String _value =
      PrefUtil.preferences!.getString(AppConstant.SORT_BY) ?? 'Sort By';
  ScrollController _scrollController = ScrollController();

  bool isLoading = false;

  @override
  void initState() {
    RepoNotifierViewModel repoNotifier =
        Provider.of<RepoNotifierViewModel>(context, listen: false);
    _scrollController.addListener(_onScroll);

    if (PrefUtil.preferences!.getBool(AppConstant.FIRST_TIME) == null) {
      PrefUtil.preferences!
          .setString(AppConstant.API_REQUEST_TIME, DateTime.now().toString());
      PrefUtil.preferences!.setInt(AppConstant.CURRENT_REPO_PAGE_INDEX, 1);
      PrefUtil.preferences!.setBool(AppConstant.FIRST_TIME, false);
      repoNotifier.fetchNetworkData(context);
    } else {
      repoNotifier.getDataFromLocal();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    RepoNotifierViewModel repoNotifier =
        Provider.of<RepoNotifierViewModel>(context);

    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          leading: Container(),
          title: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                child: Text(
                  "Git Repositories",
                  style: TextStyle(fontSize: 18),
                ),
                onTap: () async {
                  /*int d = await repoNotifier.dbHelper.getIsPageExistInDb(1);
                  print(d.toString());*/
                  //repoNotifier.initData(pageIndexOp: 1);
                },
              ),
              Container(
                width: 100,
                child: new DropdownButton<String>(
                  value: _value,
                  items: <DropdownMenuItem<String>>[
                    new DropdownMenuItem(
                      child: new Text(
                        'Sort By',
                        style: TextStyle(fontSize: 14),
                      ),
                      value: 'Sort By',
                    ),
                    new DropdownMenuItem(
                      child: new Text('Date', style: TextStyle(fontSize: 14)),
                      value: 'Date',
                    ),
                    new DropdownMenuItem(
                        child: new Text(
                          'Star Count',
                          style: TextStyle(fontSize: 14),
                        ),
                        value: 'Star Count'),
                  ],
                  onChanged: (String? value) {
                    print("object88${value}");
                    PrefUtil.preferences!
                        .setString(AppConstant.SORT_BY, value!);
                    setState(() => _value = value!);
                    repoNotifier.sortBy();
                  },
                ),
              ),
            ],
          ),
        ),
        body: Stack(
          children: [
            Container(
                color: Colors.black12,
                child: ListView.builder(
                  itemCount: CommonOperation.getTimeDifference() >
                          AppConstant.LOADING_TIME
                      ? repoNotifier.getRepoList().length + 1
                      : repoNotifier.getRepoList().length,
                  controller: _scrollController,
                  itemBuilder: (BuildContext context, int index) {
                    if (index < repoNotifier.getRepoList().length) {
                      return Padding(
                        padding: EdgeInsets.all(0),
                        key: ObjectKey(repoNotifier.getRepoList()[index]),
                        child: RepoView(
                          refo: repoNotifier.getRepoList()[index],
                          index: index,
                        ),
                      );
                    } else if (CommonOperation.getTimeDifference() >
                        AppConstant.LOADING_TIME) {
                      return _buildLoadingIndicator();
                    } else {
                      return Container();
                    }
                  },
                )),
            repoNotifier.getLoading()
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Container()
          ],
        ));
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> _onScroll() async {
    if ((_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent)) {
      RepoNotifierViewModel repoNotifier =
          Provider.of<RepoNotifierViewModel>(context, listen: false);
      bool value = await repoNotifier.hasNetwork();
      if (value) {
        if (CommonOperation.getTimeDifference() > AppConstant.LOADING_TIME) {
          RepoNotifierViewModel repoNotifier =
              Provider.of<RepoNotifierViewModel>(context, listen: false);
          repoNotifier.fetchNetworkData(context);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Wait 30 minutes for next sync")));
        }
      } else {
        repoNotifier.getDataFromLocal();
      }
    }
  }

  Widget _buildLoadingIndicator() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      /*child: Center(
        child: CircularProgressIndicator(),
      ),*/
    );
  }
}
