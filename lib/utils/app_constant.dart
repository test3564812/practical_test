class AppConstant {
  static final String CURRENT_REPO_PAGE_INDEX = "current_repo_page_index";
  static final String API_REQUEST_TIME = "api_request_time";
  static final String FIRST_TIME = "first_type";
  static final String SORT_BY = "sort_by";
  static final double LOADING_TIME = 1800.0;
}
