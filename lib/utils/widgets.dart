import 'package:flutter/material.dart';

class CustomWidgets {
  static Widget customTV(String value, {double? font_size, Color? color}) {
    return Text(
      value,
      style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: font_size ?? 18,
          color: color ?? Colors.black),
    );
  }
}
