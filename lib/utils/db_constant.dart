class DatabaseHelper{
  static final String TABLE_NAME = "repo";
  static final String NAME = "name";
  static final String LOGIN = "login";
  static final String AVATAR_URL = "avatar_url";
  static final String ID = "id";
  static final String DESCRIPTION = "description";
  static final String UPDATED_AT = "updated_at";
  static final String STAR_COUNT = "stargazers_count";
  static final String PAGE_INDEX = "page_index";
}