import 'package:intl/intl.dart';
import 'package:practical_test/utils/PrefUtil.dart';
import 'package:practical_test/utils/app_constant.dart';

class CommonOperation {
  static getLocalTime(String dateTime) {
    return DateFormat('yyyy-MMMM-dd hh:mm a')
        .format(DateTime.parse("${dateTime}"));
  }

  static int getTimeDifference() {
    String? dateTimePrevious =
        PrefUtil.preferences!.getString(AppConstant.API_REQUEST_TIME);
    final dateTime = DateTime.parse('${dateTimePrevious}');
    final daetTimeCurrent = DateTime.now();
    final difference = daetTimeCurrent.difference(dateTime).inSeconds;
    print("difference ${difference}");

    return difference;
  }
}
