# practical_test

# About

It simply loads all git repositories data from Github API and render the repositories on the screen.

- MVVM pattern is used.
- Provider approach is used for state management.

# Architecture

![Flutter MVVM + Provider Architecture](https://gitlab.com/test3564812/practical_test/-/blob/main/assets/architecture.png)
![Home Screen](https://gitlab.com/test3564812/practical_test/-/blob/main/assets/landing.jpg)
![Details Screen](https://gitlab.com/test3564812/practical_test/-/blob/main/assets/details.jpg)

# Complete and incomplete task.
Requirements Section:
    -> I have completed all 1 to 10 tasks.
Extra Credits:
    -> I have touched 3,4 and 5.

# File Structure

```
lib
  ├── components                # Components
  │   ├── repository            # repository details Views and View Model
  |
  ├── models                    # Model classes
  |
  ├── notifiers                 # Notifier Classes             
  |
  ├── screens                   # screens
  |
  ├── services                  # services
  |
  └── main.dart              
```
